importScripts('https://www.gstatic.com/firebasejs/8.9.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.9.0/firebase-analytics.js');
importScripts('https://www.gstatic.com/firebasejs/8.9.0/firebase-messaging.js')


if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('../firebase-messaging-sw.js')
  .then(function(registration) {
    console.log('Registration successful, scope is:', registration.scope);
  }).catch(function(err) {
    console.log('Service worker registration failed, error:', err);
  });
}

var firebaseConfig = {
  apiKey: "AIzaSyBJwKbnbGfAYypYHf6VxmlBQ_OQEm-kRJ8",
  authDomain: "mh-dev-46e40.firebaseapp.com",
  projectId: "mh-dev-46e40",
  storageBucket: "mh-dev-46e40.appspot.com",
  messagingSenderId: "969695637391",
  appId: "1:969695637391:web:2979ab4f84a7dd28e1a5fa",
  measurementId: "G-HFNYNF3SRC"
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging()

messaging.onBackgroundMessage((payload) => {
  console.log('[firebase-messaging-sw.js-onBackgroundMessage] Received background message ', payload);
  const { title, body, image } = {... payload.data, ...payload.notification}
  // Customize notification here
  const notificationOptions = {
    body: body,
    icon: image
  };

  self.registration.showNotification(title, notificationOptions);
})