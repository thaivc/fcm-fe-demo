# FCM
- https://github.com/firebase/quickstart-js/blob/master/messaging/README.md
## Example
- https://viblo.asia/p/push-notification-cho-react-app-su-dung-firebase-ket-hop-backend-laravel-vyDZO6zRKwj
- https://github.com/pavelpashkovsky/react-fcm
- https://github.com/firebase/quickstart-js/tree/master/messaging

## Guide
- create public/firebase-messaging-sw.js

## NOTE
- version 9: https://ittutoria.net/tips-on-solving-the-error-export-default-imported-as-firebase-was-not-found-in-firebase-app/#:~:text=To%20solve%20the%20%E2%80%9Cexport%20'default,compatibility%2C%20according%20to%20Firebase%20docs.

## NOTE v2
```
NOTE: If your payload has a notification object, setBackgroundMessageHandler will not trigger
```

```
When the app has the browser focus, 
the received message is handled through the onMessage callback in index.html.
When the app does not have browser focus then the setBackgroundMessageHandler 
callback in firebase-messaging-sw.js is where the received message is handled.
```