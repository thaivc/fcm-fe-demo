import './App.css';
import { useEffect } from 'react'
import { messaging } from './firebase'
import 'react-notifications/lib/notifications.css';
import { NotificationContainer, NotificationManager } from 'react-notifications';

function App() {

  useEffect(() => {
    messaging.requestPermission()
    .then(() => {
      return messaging.getToken()
    })
    .then(token => {
       console.log('token', token)
    })
  }, [])

  messaging.onMessage((payload) => {
    console.log('on messaging', payload)
    // const { title, body, image } = payload.notification
    const { title, body, image } = {... payload.data, ...payload.notification}
    NotificationManager.warning(body, title, 3000);
  })

  return (
    <div className="App">
      <NotificationContainer/>
      <header className="App-header">
        FCM demo
      </header>
    </div>
  );
}

export default App;
