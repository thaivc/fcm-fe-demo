
import firebase from "firebase/app";
import "firebase/messaging";

// TODO: Replace the following with your app's Firebase project configuration
// See: https://firebase.google.com/docs/web/learn-more#config-object
const firebaseConfig = {
  apiKey: "AIzaSyBJwKbnbGfAYypYHf6VxmlBQ_OQEm-kRJ8",
  authDomain: "mh-dev-46e40.firebaseapp.com",
  projectId: "mh-dev-46e40",
  storageBucket: "mh-dev-46e40.appspot.com",
  messagingSenderId: "969695637391",
  appId: "1:969695637391:web:2979ab4f84a7dd28e1a5fa",
  measurementId: "G-HFNYNF3SRC"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);


// Initialize Firebase Cloud Messaging and get a reference to the service
const messaging = firebase.messaging();
messaging.vapidKey = 'BBpP4rrzOqOv_TdKz6jI-CIcKFI6ZeAQ7Z-RhEnoUVDDgyEI9wX2EMlKDYoQXA0XJIoF6xDtfkq9_XLQJTtp7lk'
export {
  messaging
}
export default firebase;